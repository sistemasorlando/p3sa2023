
const express = require('express');
const app = express();
const axios = require('axios');
var body_parser = require('body-parser').json();

const PORT = 3000;
const HOST = 'localhost';

app.get('/', function(req,res){
    res.send("Servidor: CLIENTE");
})

app.get('/hacerPedido', function(req, res){
    var numeroPedido = Math.floor(Math.random()*(10000-1)+1);
    var mensaje = "Cliente hace pedido con pedido No. "+ numeroPedido;
    axios.post('http://localhost:3003/log',{'mensaje':mensaje});
    axios.post('http://localhost:3001/recibirPedido',{'id':numeroPedido});

    res.send({
        pedido: numeroPedido
    })
});

app.get('/getEstadoPedido/:id', body_parser, function(req,res){

    var numeroPedido = req.params.id;
    var mensaje = "Cliete consulta Estado del Pedido:"+numeroPedido;
    axios.post('http://localhost:3003/log',{'mensaje':mensaje});
    axios.get('http://localhost:3001/getEstadoPedido/'+numeroPedido)
        .then(function(response){
            res.json({'estado':" Pedido: "+numeroPedido+" "+response.data.data});
        }).catch(function(err){
            console.error(err);
        })
        .then(function(){
        });
});

app.listen(PORT,()=>{
    console.log('Servidor: CLIENTE');
});