
const express = require('express');
const app = express();
const axios = require('axios');
var body_parser = require('body-parser').json();

const PORT = 3001;
const HOST = 'localhost';

var estados = Array("RECIBIDO", "EN PRODUCCION", "CANCELADO", "ENVIADO_REPARTIDOR");

app.get('/', function(req,res){
    res.send("Servidor: RESTAURANTE");
})

app.post('/recibirPedido',body_parser , function(req, res){
    var numeroPedido = req.body.id;

    var mensaje = "Restaurante recibe pedido del cliente No. "+ numeroPedido;
    axios.post('http://localhost:3003/log',{'mensaje':mensaje});
    res.send({
        pedido: numeroPedido
    })
});

app.get('/getEstadoPedido/:id',body_parser, function(req,res){
    var numeroPedido = req.params.id;   

    let item = estados[Math.floor(Math.random()*estados.length)];
    var mensaje = "Restaurante consulta Estado del Pedido:"+numeroPedido+ " Estado: "+item;

    axios.post('http://localhost:3003/log',{'mensaje':mensaje});

    if(item === "ENVIADO_REPARTIDOR"){
        axios.get('http://localhost:3002/getEstadoPedido/'+numeroPedido)
        .then(function(response){
            res.json({'data':response.data.data});
        }).catch(function(err){
            console.error(err);
        })
        .then(function(){
        });
    }else{
        res.json({data: item});
    }
    
});

app.listen(PORT,()=>{
    console.log('Servidor: RESTAURANTE');
});