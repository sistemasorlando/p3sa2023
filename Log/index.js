
const express = require('express');
const app = express();
const axios = require('axios');
const fs = require('fs');
var body_parser = require('body-parser').json();

const PORT = 3003;
const HOST = 'localhost';

app.get('/', function(req,res){
    res.send("Servidor: LOG");
})

app.post('/log', body_parser,function(req, res){
    var mensaje = req.body.mensaje;
    var fecha = new Date();
    var l = "Log: "+mensaje+" "+fecha+"\n";
    var log = fs.createWriteStream('server.log',{
        flags: 'a'
    });
    log.write(l);
    log.end();
    res.send("OK");
});

app.listen(PORT,()=>{
    console.log('Servidor: LOG');
});