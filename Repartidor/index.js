
const express = require('express');
const app = express();
const axios = require('axios');
var body_parser = require('body-parser').json();

const PORT = 3002;
const HOST = 'localhost';

var estados = Array("RECIBIDA_RESTAURANTE" ,"EN CAMINO", "ENTREGADO");

app.get('/', function(req,res){
    res.send("Servidor: REPARTIDOR");
});

app.get('/getEstadoPedido/:id', body_parser, function(req,res){
    var numeroPedido = req.params.id;
    let item = estados[Math.floor(Math.random()*estados.length)];
    var mensaje = "Repartidor Informa Estado del Pedido:"+numeroPedido +" Estado: "+item;
    axios.post('http://localhost:3003/log',{'mensaje':mensaje});
    if(item === "ENTREGADO"){
        axios.post('http://localhost:3002/entregarPedido',{'pedido':numeroPedido});
    }
    res.json({'data':item});
});

app.post('/entregarPedido', body_parser ,function(req, res){
    var numeroPedido = req.body.pedido;

    var mensaje = "Repartidor confirma Entrega del pedido: "+ numeroPedido;
    axios.post('http://localhost:3003/log',{'mensaje':mensaje});

    res.send({
        pedido: numeroPedido
    })
});


app.listen(PORT,()=>{
    console.log('Servidor: REPARTIDOR localhost:3002');
});